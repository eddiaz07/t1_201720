package model.logic;

import java.util.Iterator;

import model.data_structures.NumbersBag;


public class NumbersBagOperations {

	
	
	public double computeMean(NumbersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Double> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public double getMax(NumbersBag bag){
		double max = Double.MIN_VALUE;
	    double value;
		if(bag != null){
			Iterator <Double> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	public double getMin(NumbersBag bag){
		double min = Double.MAX_VALUE;
	    double value;
		if(bag != null){
			Iterator<Double> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}
			
		}
		return min;
	}
	public double getMultiplication(NumbersBag bag){
	
	    double value=0;
		if(bag != null){
			Iterator<Double> iter = bag.getIterator();
			while(iter.hasNext()){
				value *= iter.next();
				}
			}
			
		
		return value;
	}


}
