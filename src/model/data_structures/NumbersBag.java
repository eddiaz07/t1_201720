package model.data_structures;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;


public class NumbersBag <T extends Comparable<T>> {
	
	private HashSet<Integer> bag;
	
	public NumbersBag(){
		this.bag = new HashSet<Integer>();
	}
	
	public NumbersBag(ArrayList<Integer> data){
		this();
		if(data != null){
			for (Integer datum : data) {
				bag.add(datum);
			}
		}
		
	}
	
	
	public void addDatum(Integer datum){
		bag.add(datum);
	}
	
	public Iterator<Integer> getIterator(){
		return this.bag.iterator();
	}

}
